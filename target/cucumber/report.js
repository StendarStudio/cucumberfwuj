$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ContactUs.feature");
formatter.feature({
  "line": 1,
  "name": "Submit data to webdriveruniversity.com using contact_us form",
  "description": "A user should be to submit information via the contact_us form.",
  "id": "submit-data-to-webdriveruniversity.com-using-contact-us-form",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4434010894,
  "status": "passed"
});
formatter.before({
  "duration": 3444612874,
  "status": "passed"
});
formatter.before({
  "duration": 3576715829,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I access webdriveruniversity.com",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I click on the contact_us button",
  "keyword": "When "
});
formatter.match({
  "location": "ContactUsSteps.iAccessWebdriveruniversity()"
});
formatter.result({
  "duration": 4373392057,
  "status": "passed"
});
formatter.match({
  "location": "ContactUsSteps.iClickOnTheContact_usButton()"
});
formatter.result({
  "duration": 403414093,
  "status": "passed"
});
formatter.scenario({
  "line": 8,
  "name": "Submit valid data via contact_us form",
  "description": "",
  "id": "submit-data-to-webdriveruniversity.com-using-contact-us-form;submit-valid-data-via-contact-us-form",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 7,
      "name": "@chrome"
    }
  ]
});
formatter.step({
  "line": 9,
  "name": "I enter a valid first name",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I enter a valid last name",
  "rows": [
    {
      "cells": [
        "Jenkins",
        "Python",
        "Cucumber"
      ],
      "line": 11
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I enter a valid email address",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I enter comments",
  "rows": [
    {
      "cells": [
        "example comment 1",
        "example comment 2"
      ],
      "line": 14
    },
    {
      "cells": [
        "example comment 3",
        "example comment 4"
      ],
      "line": 15
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I click on the submit button",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "The information should successfully be submitted via the contact_us form",
  "keyword": "Then "
});
formatter.match({
  "location": "ContactUsSteps.iEnterAValidFirstName()"
});
formatter.result({
  "duration": 1221682327,
  "status": "passed"
});
formatter.match({
  "location": "ContactUsSteps.iEnterAValidLastName(DataTable)"
});
formatter.result({
  "duration": 110081059,
  "status": "passed"
});
formatter.match({
  "location": "ContactUsSteps.iEnterAValidEmailAddress()"
});
formatter.result({
  "duration": 199648193,
  "status": "passed"
});
formatter.match({
  "location": "ContactUsSteps.iEnterComments(DataTable)"
});
formatter.result({
  "duration": 258167319,
  "status": "passed"
});
formatter.match({
  "location": "ContactUsSteps.iClickOnTheSubmitButton()"
});
formatter.result({
  "duration": 1121672695,
  "status": "passed"
});
formatter.match({
  "location": "ContactUsSteps.theInformationShouldSuccessfullyBeSubmittedViaTheContact_usForm()"
});
formatter.result({
  "duration": 3059547467,
  "status": "passed"
});
formatter.uri("Login.feature");
formatter.feature({
  "line": 1,
  "name": "Login into account",
  "description": "",
  "id": "login-into-account",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Login to account with credentials",
  "description": "",
  "id": "login-into-account;login-to-account-with-credentials",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "The user navigates to \"\u003curl\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "The user clicks on the login portal button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "The user entres the \"\u003cusername\u003e\" username",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "The user enter the \"\u003cpassword\u003e\" password",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "The user clicks on the login button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "The user should be presented with the following prompt alert \"\u003cmessage\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "line": 11,
  "name": "",
  "description": "",
  "id": "login-into-account;login-to-account-with-credentials;",
  "rows": [
    {
      "cells": [
        "url",
        "username",
        "password",
        "message"
      ],
      "line": 12,
      "id": "login-into-account;login-to-account-with-credentials;;1"
    },
    {
      "cells": [
        "http://www.webdriveruniversity.com/",
        "Jenkins",
        "pytjen",
        "validation failed"
      ],
      "line": 13,
      "id": "login-into-account;login-to-account-with-credentials;;2"
    },
    {
      "cells": [
        "http://www.webdriveruniversity.com/",
        "webdriver",
        "webdriver123",
        "validation succeeded"
      ],
      "line": 14,
      "id": "login-into-account;login-to-account-with-credentials;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 3703619445,
  "status": "passed"
});
formatter.before({
  "duration": 4028484852,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Login to account with credentials",
  "description": "",
  "id": "login-into-account;login-to-account-with-credentials;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "The user navigates to \"http://www.webdriveruniversity.com/\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "The user clicks on the login portal button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "The user entres the \"Jenkins\" username",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "The user enter the \"pytjen\" password",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "The user clicks on the login button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "The user should be presented with the following prompt alert \"validation failed\"",
  "matchedColumns": [
    3
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "http://www.webdriveruniversity.com/",
      "offset": 23
    }
  ],
  "location": "LoginSteps.theUserNavigatesTo(String)"
});
formatter.result({
  "duration": 3778387940,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.theUserClicksOnTheLoginPortalButton()"
});
formatter.result({
  "duration": 3189518736,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Jenkins",
      "offset": 21
    }
  ],
  "location": "LoginSteps.theUserEntresTheUsername(String)"
});
formatter.result({
  "duration": 1325485176,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "pytjen",
      "offset": 20
    }
  ],
  "location": "LoginSteps.theUserEnterThePassword(String)"
});
formatter.result({
  "duration": 227591045,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.theUserClicksOnTheLoginButton()"
});
formatter.result({
  "duration": 195613780,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "validation failed",
      "offset": 62
    }
  ],
  "location": "LoginSteps.theUserShouldbePresentedWithTheFollowingPromptAlert(String)"
});
formatter.result({
  "duration": 28795131,
  "status": "passed"
});
formatter.before({
  "duration": 8007194046,
  "status": "passed"
});
formatter.before({
  "duration": 6787151143,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "Login to account with credentials",
  "description": "",
  "id": "login-into-account;login-to-account-with-credentials;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "The user navigates to \"http://www.webdriveruniversity.com/\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "The user clicks on the login portal button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "The user entres the \"webdriver\" username",
  "matchedColumns": [
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "The user enter the \"webdriver123\" password",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "The user clicks on the login button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "The user should be presented with the following prompt alert \"validation succeeded\"",
  "matchedColumns": [
    3
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "http://www.webdriveruniversity.com/",
      "offset": 23
    }
  ],
  "location": "LoginSteps.theUserNavigatesTo(String)"
});
formatter.result({
  "duration": 8194944667,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.theUserClicksOnTheLoginPortalButton()"
});
formatter.result({
  "duration": 3211333385,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "webdriver",
      "offset": 21
    }
  ],
  "location": "LoginSteps.theUserEntresTheUsername(String)"
});
formatter.result({
  "duration": 2774462084,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "webdriver123",
      "offset": 20
    }
  ],
  "location": "LoginSteps.theUserEnterThePassword(String)"
});
formatter.result({
  "duration": 145049396,
  "status": "passed"
});
formatter.match({
  "location": "LoginSteps.theUserClicksOnTheLoginButton()"
});
formatter.result({
  "duration": 138796845,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "validation succeeded",
      "offset": 62
    }
  ],
  "location": "LoginSteps.theUserShouldbePresentedWithTheFollowingPromptAlert(String)"
});
formatter.result({
  "duration": 18946742,
  "status": "passed"
});
formatter.uri("Products.feature");
formatter.feature({
  "line": 1,
  "name": "Products",
  "description": "",
  "id": "products",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "Validate promo code alert is visible when clicking on the special offers link",
  "description": "",
  "id": "products;validate-promo-code-alert-is-visible-when-clicking-on-the-special-offers-link",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "The user navigates to \"\u003chttp\u003e\" url",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Ihe user clicks on \"\u003cbutton\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "The user should be presented with a promo ofert",
  "keyword": "Then "
});
formatter.examples({
  "line": 8,
  "name": "",
  "description": "",
  "id": "products;validate-promo-code-alert-is-visible-when-clicking-on-the-special-offers-link;",
  "rows": [
    {
      "cells": [
        "http",
        "button"
      ],
      "line": 9,
      "id": "products;validate-promo-code-alert-is-visible-when-clicking-on-the-special-offers-link;;1"
    },
    {
      "cells": [
        "http://www.webdriveruniversity.com/Page-Object-Model/products.html",
        "#container-special-offers"
      ],
      "line": 10,
      "id": "products;validate-promo-code-alert-is-visible-when-clicking-on-the-special-offers-link;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 6547007231,
  "status": "passed"
});
formatter.before({
  "duration": 6652931108,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Validate promo code alert is visible when clicking on the special offers link",
  "description": "",
  "id": "products;validate-promo-code-alert-is-visible-when-clicking-on-the-special-offers-link;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 4,
  "name": "The user navigates to \"http://www.webdriveruniversity.com/Page-Object-Model/products.html\" url",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "Ihe user clicks on \"#container-special-offers\"",
  "matchedColumns": [
    1
  ],
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "The user should be presented with a promo ofert",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "http://www.webdriveruniversity.com/Page-Object-Model/products.html",
      "offset": 23
    }
  ],
  "location": "ProductsSteps.user_navigates_to_website(String)"
});
formatter.result({
  "duration": 22620152486,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "#container-special-offers",
      "offset": 20
    }
  ],
  "location": "ProductsSteps.user_clicks_on(String)"
});
formatter.result({
  "duration": 2659937645,
  "status": "passed"
});
formatter.match({
  "location": "ProductsSteps.user_should_be_presented_with_a_promo_alert()"
});
formatter.result({
  "duration": 3170406197,
  "status": "passed"
});
});