package runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions (
        features = {"src/test/java/featureFiles"},
        glue = {"steps"},
        monochrome = true,
        //dryRun = false,
        tags = {},
        plugin = {"pretty:target/cucumber-pretty.txt",
                "html:target/cucumber",
                "json:target/cucumber.json",
               // "com.cucumber.listener.ExtentCucumberFormatter:output/report.html"
                }
        )

public class MainRunner extends AbstractTestNGCucumberTests {
}



