package pageObjects;

import cucumber.api.DataTable;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.io.IOException;
import java.util.List;

public class ContactUs_Page extends BasePage {
    public @FindBy(xpath = "//input[@name='first_name']") WebElement textfield_FirstName;
    public @FindBy(id = "contact-us") WebElement link_ContactUs;
    public @FindBy(xpath = "//input[@name='last_name']") WebElement textfield_LastName;
    public @FindBy(xpath = "//input[@name='email']") WebElement textfield_EmailAddress;
    public @FindBy(xpath = "//textarea[@name='message']") WebElement textfield_Message;
    public @FindBy(xpath = "//input[@value='SUBMIT']") WebElement button_Submit;

    public ContactUs_Page() throws IOException {
        super();
    }

    public ContactUs_Page getWwWPage() throws IOException {
        getDriver().get("http://www.webdriveruniversity.com/");
        return new ContactUs_Page();
    }
    public ContactUs_Page clickOnContactUsLink() throws Exception {
       link_ContactUs.click();
       return new ContactUs_Page();
    }
    public ContactUs_Page enterFirstName() throws Exception {
        sendKeysToWebElement(textfield_FirstName, "Zdzislaw");
        return new ContactUs_Page();
    }
    public ContactUs_Page enterLastName(DataTable dataTable, int row, int column) throws Exception {
        List<List<String>> data = dataTable.raw();
        sendKeysToWebElement(textfield_LastName, data.get(row).get(column));
        return new ContactUs_Page();
    }
    public ContactUs_Page enterEmailAddress(String email) throws Exception {
        sendKeysToWebElement(textfield_EmailAddress, email);
        return new ContactUs_Page();
    }
    public ContactUs_Page enterComment(DataTable dataTable, int row, int column) throws Exception {
        List<List<String>> data = dataTable.raw();
        sendKeysToWebElement(textfield_Message, data.get(row).get(column));
        return new ContactUs_Page();
    }
    public ContactUs_Page clickOnSubmitButton() throws Exception {
        waitAndClickElement(button_Submit);
        return new ContactUs_Page();
    }
    public ContactUs_Page confirmContacyUsFormSubmissionWasSuccessful()throws IOException {
        WebElement thanksContactUsPage = getDriver().findElement(By.xpath("//h1"));
        waitUntilWebElementIsVisible(textfield_Message);
        Assert.assertEquals("Thank You for your Message!", thanksContactUsPage.getText());
        return new ContactUs_Page();
    }
}
