package utils;

import java.io.Reader;

public class Constant {

    /**Config Properties file **/
    public final static String CONFIG_PROPERTIES_DIRECTORY = "properties\\config.properties";

    public final static String GECKO_DRIVER_DIRECTORY = System.getProperty("user.dir") + "\\src\\test\\java\\drivers\\geckodriver.exe";

    public final static String CHROME_DRIVER_DIRECTORY = System.getProperty("user.dir") + "\\src\\test\\java\\drivers\\chromedriver.exe";

    public final static String IE_DRIVER_DIRECTORY = System.getProperty("user.dir") + "\\src\\test\\java\\drivers\\IEDriverServer.exe";
}
