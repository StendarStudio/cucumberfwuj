package steps;

import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

//import static utils.DriverFactory.contactUsPage;

public class ContactUsSteps {

    WebDriver webDriver;

    @Before("@chrome")
    public void setupChrome() throws IOException {
        System.setProperty("webdriver.chrome.driver",
                Paths.get(System.getProperty("user.dir")).toRealPath() + "\\src\\test\\java\\drivers\\chromedriver.exe");
        this.webDriver = new ChromeDriver();
        this.webDriver.manage().window().maximize();
        this.webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
    }

//        @Before("@fox")
//    public void setupFirefox() throws IOException {
//        System.setProperty("webdriver.geco.driver",
//                Paths.get(System.getProperty("user.dir")).toRealPath() + "\\src\\test\\java\\drivers\\gecodriver.exe");
//        this.webDriver = new FirefoxDriver();
//        this.webDriver.manage().window().maximize();
//        this.webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
//    }


    @After
    public void closeDriver() {
        //webDriver.manage().deleteAllCookies();
        webDriver.close();
        webDriver.quit();
    }

   @Given("^I access webdriveruniversity.com$")
    public void iAccessWebdriveruniversity() throws Throwable {
       contactUsPage.getWwWPage();
    }

    @When("^I click on the contact_us button$")
    public void iClickOnTheContact_usButton() throws Throwable {
        contactUsPage.clickOnContactUsLink();
        //webDriver.findElement(By.id("contact-us")).click();
    }

    @And("^I enter a valid first name$")
    public void iEnterAValidFirstName() throws Throwable {
        contactUsPage.enterFirstName();
    }

    @And("^I enter a valid last name$")
    public void iEnterAValidLastName(DataTable dataTable) throws Throwable {
        contactUsPage.enterLastName(dataTable, 0, 1);
    }

    @And("^I enter a valid email address$")
    public void iEnterAValidEmailAddress() throws Throwable {
        contactUsPage.enterEmailAddress("zdzislaw.python@gmail.com");
        }

    @And("^I enter comments$")
    public void iEnterComments(DataTable dataTable) throws Throwable {
        contactUsPage.enterComment(dataTable, 0, 0);
        contactUsPage.enterComment(dataTable, 1, 1);
        }

    @When("^I click on the submit button$")
    public void iClickOnTheSubmitButton() throws Throwable {
        contactUsPage.clickOnSubmitButton();
    }

    @Then("^The information should successfully be submitted via the contact_us form$")
    public void theInformationShouldSuccessfullyBeSubmittedViaTheContact_usForm() throws Throwable {
        contactUsPage.confirmContacyUsFormSubmissionWasSuccessful();
    }
}