package steps;

//import cucumber.api.java.Before;
//import org.junit.After;
import org.openqa.selenium.By;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
import utils.DriverFactory;
//import java.io.IOException;
//import java.nio.file.Paths;
//import java.util.concurrent.TimeUnit;

public class ProductsSteps extends DriverFactory {


//    @Before
//    public void setupChrome() throws IOException {
//        System.setProperty("webdriver.chrome.driver",
//                Paths.get(System.getProperty("user.dir")) + "\\src\\test\\java\\drivers\\chromedriver.exe");
//        this.webDriver = new ChromeDriver();
//        this.webDriver.manage().window().maximize();
//        this.webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
//    }
//
//    @After
//    public void closeDriver() {
//        //webDriver.manage().deleteAllCookies();
//        webDriver.close();
//        webDriver.quit();
//    }

    @Given("^The user navigates to \"([^\"]*)\" url$")
    public void user_navigates_to_website(String http) throws InterruptedException {
        Thread.sleep(2000);
        getDriver().get(http);
    }

    @When("^Ihe user clicks on \"([^\"]*)\"$")
    public void user_clicks_on(String locator) throws InterruptedException {
        Thread.sleep(2000);
        getDriver().findElement(By.cssSelector(locator)).click();
    }

    @Then("^The user should be presented with a promo ofert$")
    public void user_should_be_presented_with_a_promo_alert() throws InterruptedException  {
        Thread.sleep(2000);
        getDriver().findElement(By.xpath("//button[text()='Proceed']")).click();
    }

}
