package steps;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.After;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertEquals;



public class LoginSteps  {

    WebDriver webDriver;

    @Before
    public void setupChrome() throws IOException {
        System.setProperty("webdriver.chrome.driver",
                Paths.get(System.getProperty("user.dir")).toRealPath() + "\\src\\test\\java\\drivers\\chromedriver.exe");
        this.webDriver = new ChromeDriver();
        this.webDriver.manage().window().maximize();
        this.webDriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
    }

    @After
    public void closeDriver() {
        //webDriver.manage().deleteAllCookies();
        webDriver.close();
        webDriver.quit();
    }

    @Given("^The user navigates to \"([^\"]*)\"$")
    public void theUserNavigatesTo(String url) throws Throwable {
        Thread.sleep(2000);
        webDriver.get(url);
    }

    @When("^The user clicks on the login portal button$")
    public void theUserClicksOnTheLoginPortalButton() throws Throwable {
        Thread.sleep(3000);
        webDriver.findElement(By.id("login-portal")).click();
    }

    @And("^The user entres the \"([^\"]*)\" username$")
    public void theUserEntresTheUsername(String username) throws Throwable {
        for (String windHandle: webDriver.getWindowHandles()) {
            webDriver.switchTo().window(windHandle);
        } //TODO foreach windHandle
        webDriver.findElement(By.id("text")).sendKeys(username);
    }

    @And("^The user enter the \"([^\"]*)\" password$")
    public void theUserEnterThePassword(String password) throws Throwable {
        webDriver.findElement(By.id("password")).sendKeys(password);
    }

    @When("^The user clicks on the login button$")
    public void theUserClicksOnTheLoginButton() throws Throwable {
        webDriver.findElement(By.id("login-button")).click();
    }

    @Then("^The user should be presented with the following prompt alert \"([^\"]*)\"$")
    public void theUserShouldbePresentedWithTheFollowingPromptAlert(String message) throws Throwable {
        Alert alert = webDriver.switchTo().alert();
        assertEquals(alert.getText().toString().toLowerCase().replaceAll("\\s", ""), message.toLowerCase().replaceAll("\\s", ""));
        alert.accept();
    }
}
