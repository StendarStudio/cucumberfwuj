Feature: Submit data to webdriveruniversity.com using contact_us form
  A user should be to submit information via the contact_us form.

  Background:
    Given I access webdriveruniversity.com
    When I click on the contact_us button
@chrome
  Scenario: Submit valid data via contact_us form
    And I enter a valid first name
    And I enter a valid last name
    | Jenkins | Python | Cucumber |
    And I enter a valid email address
    And I enter comments
    | example comment 1 | example comment 2 |
    | example comment 3 | example comment 4 |
    When I click on the submit button
    Then The information should successfully be submitted via the contact_us form

#  @fox
#  Scenario: Submit non valid data via contact_us form
#    And I enter a non valid first name
#    And I enter a non valid last name
#    And I enter a non valid email address
#    And I enter comments
#    When I click on the submit button
#    Then The information should not be successfully submitted via the contact_us form
#    But The user should also be notified of the problem
