Feature: Products

  Scenario Outline: Validate promo code alert is visible when clicking on the special offers link
    Given The user navigates to "<http>" url
    When Ihe user clicks on "<button>"
    Then The user should be presented with a promo ofert

    Examples:
      |                              http                                   |          button           |
      | http://www.webdriveruniversity.com/Page-Object-Model/products.html     | #container-special-offers |
