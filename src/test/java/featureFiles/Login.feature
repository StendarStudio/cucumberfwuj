Feature: Login into account

  Scenario Outline: Login to account with credentials
    Given The user navigates to "<url>"
    When The user clicks on the login portal button
    And The user entres the "<username>" username
    And The user enter the "<password>" password
    When The user clicks on the login button
    Then The user should be presented with the following prompt alert "<message>"

    Examples:
      | url                                 | username  | password     | message              |
      | http://www.webdriveruniversity.com/ | Jenkins   | pytjen       | validation failed    |
      | http://www.webdriveruniversity.com/ | webdriver | webdriver123 | validation succeeded |